<!DOCTYPE html>
<html>
<head>
    <title>10 Green Bottles</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:300i" rel="stylesheet" />
    <style type="text/css">
        p {
            font-family: 'Roboto', sans-serif;
            font-weight: 300;
            font-style: italic;
        }
    </style>
</head>
<body>
    <?php
        require_once('app.php');
        generateLyrics();
    ?>
</body>
</html>
