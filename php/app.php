<?php

use NumberToWords\NumberToWords;

require_once('vendor/autoload.php');

$objectDescription;
$pluralObjectDescription;

function objDescription($qty) {
    global $objectDescription;
    global $pluralObjectDescription;

    if ($qty === 1) {
        return $objectDescription;
    } else {
        return $pluralObjectDescription;
    }
}

function generateLyrics(
    $startNum = 10,
    $colour = 'green',
    $object = 'bottle',
    $verb = 'hanging',
    $location = 'wall',
    $accident = 'fall'
) {
    global $objectDescription;
    global $pluralObjectDescription;

    $numberToWords = new NumberToWords();
    $numberTransformer = $numberToWords->getNumberTransformer('en');

    $objPlural = $object . 's';

    // Conjoined phrases to shorten later code and save re-use of vars
    $objectDescription = "$colour $object";
    $pluralObjectDescription = "$colour $objPlural";
    $presence = "$verb on the $location";

    // Loop from start number to 1
    for ($num = $startNum; $num > 0; $num--) {
        $wordNumber = $numberTransformer->toWords($num);
        $nextWordNumber = $numberTransformer->toWords($num - 1);

        // Changing 'zero' for 'no' if last number
        if ($num === 1) {
            $nextWordNumber = 'no';
        }

        echo '<p>';

        // echo opening statement twice
        // ie. Ten green bottles hanging on the wall,
        echo ucfirst($wordNumber) . ' ' . objDescription($num) . " $presence,<br>";
        echo ucfirst($wordNumber) . ' ' . objDescription($num) . " $presence,<br>";

        // echo third line
        // ie. And if one green bottle should accidentally fall,
        echo "And if one $objectDescription should accidentally $accident,<br>";

        // echo last line
        // ie. There'll be no green bottles hanging on the wall.
        // Last line changed to singluar if only one remaining
        echo "There'll be $nextWordNumber " .
            objDescription($num - 1) .
            " $presence.";

        echo '</p>';
    }
}
