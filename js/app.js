var app = new Vue({
    el: '#app',
    data: {
        quantity: 10,
        currentQuantity: 10,
        colour: 'green',
        object: 'bottle',
        verb: 'hanging',
        location: 'wall',
        accident: 'fall'
    },
    computed: {
        numArray: function() {
            return Array.apply(null, {length: this.quantity}).map(Function.call, Number).reverse();
        }
    },
    methods: {
        objectDescription(i) {
            if (i === 1) {
                return this.object;
            }

            return this.object + 's';
        }
    },
    filters: {
        'toWords': function(value) {
            if (!value) return '';
            return numberToWords.toWords(value);
        },
        'capitalise': function (value) {
            return value.charAt(0).toUpperCase() + value.slice(1);
        }
    }
})
