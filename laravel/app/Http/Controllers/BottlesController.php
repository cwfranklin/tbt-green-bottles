<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BottlesController extends Controller
{
    public function get()
    {
        $defaultObj = 'bottle';
        $defaults = [
            'quantity' => 10,
            'colour' => 'green',
            'object' => $defaultObj,
            'verb' => 'hanging',
            'location' => 'wall',
            'accident' => 'fall',
        ];

        return view('welcome', $defaults);
    }

    public function post(Request $request)
    {
        $request->validate([
            'quantity' => 'required|integer|min:1',
            'colour' => 'required|string',
            'object' => 'required|string',
            'verb' => 'required|string',
            'location' => 'required|string',
            'accident' => 'required|string',
        ]);

        return view('welcome', [
            'quantity' => $request->input('quantity'),
            'colour' => $request->input('colour'),
            'object' => $object = $request->input('object'),
            'verb' => $request->input('verb'),
            'location' => $request->input('location'),
            'accident' => $request->input('accident'),
        ]);
    }
}
