<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <style type="text/css">
            p {
                text-align: center;
            }
        </style>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
    </head>
    <body>
        <div class="container">
            <h1 class="mt-3">Green Bottles</h1>

            <hr>

            <form action="{{ route('bottles') }}" method="POST">
                @csrf

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="quantity">Quantity</label>
                        <input type="number" class="form-control" name="quantity" placeholder="Quantity" value="{{ old('quantity', 10) }}" required>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="colour">Colour</label>
                        <input type="text" class="form-control" name="colour" placeholder="Colour" value="{{ old('colour', 'green') }}" required>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="object">Object</label>
                        <input type="text" class="form-control" name="object" placeholder="Object" value="{{ old('object', 'bottle') }}" required>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="verb">Verb</label>
                        <input type="text" class="form-control" name="verb" placeholder="Verb" value="{{ old('verb', 'hanging') }}" required>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="location">Location</label>
                        <input type="text" class="form-control" name="location" placeholder="Location" value="{{ old('location', 'wall') }}" required>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="accident">Accident</label>
                        <input type="text" class="form-control" name="accident" placeholder="Accident" value="{{ old('accident', 'fall') }}" required>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>

            <hr>

            @php
                $pluralObj = $object . 's';

                function isPlural($qty) {
                    return $qty !== 1;
                }
            @endphp

            <div class="row">
                <div class="col-md-12">
                    @for ($num = $quantity; $num > 0; $num--)
                        @php
                            $nextNum = $num - 1;

                            $numberToWords = new \NumberToWords\NumberToWords();
                            $numberTransformer = $numberToWords->getNumberTransformer('en');

                            $numWord = ucfirst($numberTransformer->toWords($num));
                            $nextNumWord = $numberTransformer->toWords($num - 1);

                            if ($num === 1) {
                                $nextNumWord = 'no';
                            }
                        @endphp

                        <p>
                            {{ $numWord }} {{ $colour }} {{ isPlural($num) ? $pluralObj : $object }} {{ $verb }} on the {{ $location }},<br>
                            {{ $numWord }} {{ $colour }} {{ isPlural($num) ? $pluralObj : $object }} {{ $verb }} on the {{ $location }},<br>
                            And if one {{ $colour }} {{ $object }} should accidentally {{ $accident }},<br>
                            There'll be {{ $nextNumWord }} {{ $colour }} {{ isPlural($nextNum) ? $pluralObj : $object }} {{ $verb }} on the {{ $location }}.
                        </p>
                    @endfor
                </div>
            </div>
        </div>
    </body>
</html>
