<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BottlesController@get');
Route::get('/bottles', 'BottlesController@get');
Route::post('/bottles', 'BottlesController@post')->name('bottles');
